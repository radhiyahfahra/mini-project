import React, { useState } from 'react';
import { Link, withRouter } from "react-router-dom";
import {

  Form,
  FormGroup,
  Input,
  Label,
  Button,

} from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';
import verifyToken from '../../helpers/VerifyToken';
import "../../styles/SignIn.css";
const baseUrl = "https://miniproject-team-a.herokuapp.com/api/v1/"

class SignIn extends React.Component {
  state = {
    id: "",
    email: "",
    password: ""
   // isLoading: false
  }
  
  handleOnchange = (e) => {
    e.preventDefault();

    this.setState({
      [e.target.name]: e.target.value
    })
  }

handleLoginClick = async(e) => {
    e.preventDefault()
    // create data 
    const LoginUser = {
        email: this.state.email,
        password: this.state.password
    }
    try {
        const res = await axios.post(`${baseUrl}/login`, LoginUser)
        console.log(res.data)
        if(res.data.status === "success") {
            localStorage.setItem("token", res.data.data.token)
    //        this.setState({ isLoading: false, email: "", password: ""})
            this.props.history.push("/dashboard")
            console.log("check", res)
        }
    }
    catch(error){
    console.log(error)
    //this.setState({ isLoading: false, email: "", password: ""})
    } 
}


//   componentDidMount() {
//     let token = verifyToken()
//     if(token) this.props.history.replace('/')
//   }

// change = e => {
//     this.setState({
//         [e.target.name] : e.target.value
//     })
// }

// handleLoginClick = (e) => {
//     e.preventDefault();
//     console.log("check")

//     axios({
//       method: "POST",
//       url: "https://miniproject-team-a.herokuapp.com/api/v1/login",
//       data: {
//         email: this.state.email,
//         password: this.state.password
//       }
//     })
//     .then(res => {
//       if(res.data.success) {
//         // Simpen Token di local storage
//         localStorage.setItem('token', res.data.access_token)
//         // Redirect user ke home
//         this.props.history.push("/dashboard")
//         console.log("check", res)
//       }
//       else{
//         // Handle Error yang masuk ke then
//       }
//     })
//     .catch(err => {
//         console.log(err)
//      //   this.setState({isLoading:true})

//     //     Swal.fire({
//     //     icon: "error",
//     //     text: err.response.data.message
//     //   })
//     })

//   }

  render() {
    return (
        <Form>
            <div className="biru">
        <div className="retangle">          
           <h2 className="hello">Hello, Friend!</h2>
           <p className="todos">Todos</p>
           <p className="detail">Enter your personal details and</p>
           <Link to="/signup">
             <button className="signup-bot">SIGN UP</button>
          </Link>
        </div>  
        <div className="">
            <h4 className="signin">Sign in to Task Manager</h4>
            <p className="use-email">or use your email account</p>
            <button className="fb-border"></button>
            <button className="gg-border"></button>
            <button className="in-border"></button>
              
        <div className>
            <p className="fb-icon">f</p>
            <p className="gg-icon">G+</p>
            <p className="in-icon">In</p>
        </div>    
    </div>
    </div>
            <FormGroup >
            <Label for="email"></Label>
            <Input className="email-input1"
                type="email" 
                name="email" 
                value={this.state.email} 
                onChange={this.handleOnchange}
                placeholder="Enter Your Email" />
            <Label for="password"></Label>
            <Input className="pw-input1"
                type="password" 
                name="password"
                value={this.state.password}
                onChange={this.handleOnchange}
                placeholder="Enter Your Password" />
            
            <Button className="signin-bot" color="primary" onClick={this.handleLoginClick}>Login</Button>
            </FormGroup>
        </Form>
      )
    }
}
export default withRouter(SignIn);