import React, { useState } from 'react';
import { Link } from "react-router-dom";
import {
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Button,
  Card,
  CardBody,
  CardTitle
} from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';
import verifyToken from '../../helpers/VerifyToken';
import "../../styles/SignUp.css";
const baseUrl = "https://miniproject-team-a.herokuapp.com/api/v1/tasks"


class SignUp extends React.Component {
  state = {
    email: "",
    name: "",
    password: "",
    isLoading: false    
  } //bikin state untuk body register nanti
  

//   componentDidMount() {
//   let token = localStorage.getItem('token')
// //     if(token) this.props.history.replace('/')
//   }

  handleOnchange = (e) => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleRegisterClick = (e) => {
    this.setState({isLoading:true})
    e.preventDefault();

    axios({
      method: 'POST',
        url: "https://miniproject-team-a.herokuapp.com/api/v1/register",
        data: {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password
        }
    })
    .then(res => {
      console.log('signin', res)
            {
                alert('Register Success')
                localStorage.setItem('token', res.data.access_token)
                this.props.history.push('/signin')
            }
    })

    .catch(err => {
      this.setState({loading: false})
      console.log(err)

      // Swal.fire({
      //   icon: 'error',
      //   text: err.response.data.message })
    }
  )}
    
    
  
  render() {
    return (
        <Form>
            <div className="biru">
        <div className="retangle">          
           <h2 className="hello">Welcome Back!</h2>
           <p className="todos">Todos</p>
           <p className="detail">To keep connected with us please</p>
           <p className="detail1">login with your personal info</p>
           <Link to="/signin">
             <button className="signin-bot1">SIGN IN</button>
          </Link>
        </div>  
        <div className="">
            <h4 className="signin">Created Account</h4>
            <p className="use-email">or use your email account</p>
            <button className="fb-border"></button>
            <button className="gg-border"></button>
            <button className="in-border"></button>
              
        <div className>
            <p className="fb-icon">f</p>
            <p className="gg-icon">G+</p>
            <p className="in-icon">In</p>
        </div>    
    </div>
    </div>
            <FormGroup>
            <Label for="name"></Label>
            <Input className="name-input"
                type="name" 
                name="name"
                value={this.state.name}
                onChange={this.handleOnchange}
                placeholder="Enter Your Name" />
            <Label for="email"></Label>
            <Input className="email-input"
                type="email" 
                name="email" 
                value={this.state.email} 
                onChange={this.handleOnchange}
                placeholder="Enter Your Email" />
            <Label for="password"></Label>
            <Input className="pw-input"
                type="password" 
                name="password"
                value={this.state.password}
                onChange={this.handleOnchange}
                placeholder="Enter Your Password" 
                
                />
            </FormGroup>
            
        <Button className="signup-bot1" color="primary" onClick={this.handleRegisterClick}>SIGN UP</Button>
        </Form>
      )
    }
}
export default SignUp;