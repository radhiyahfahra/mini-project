import React, { Fragment } from "react";
import axios from "axios";
import AddForm from "../../components/AddForm";

const baseUrl = "https://miniproject-team-a.herokuapp.com/api/v1";
let token;

class CountryDetail extends React.Component{
  state = {
    tasks: [],
    showModal: false
  }

  logout = e => {
    localStorage.removeItem("token");
    this.props.history.push("/")
  }

  getAllTask = async() => {
    token = localStorage.getItem("token")
    try {
      const res = await axios.get(`${baseUrl}/tasks`,{
        headers: {
          auth: token
        }
      })
      this.setState({ tasks: res.data.data.Task })
    }catch(err) {
      console.log(err)
    }
  } 

  // deleteItem = async(id) => {
  //   token = localStorage.getItem("token")
  //   try {
  //     const res = await axios.delete(`${baseUrl}/tasks/${name}`,{
  //       headers: {
  //         auth: token
  //       }
  //     })
  //     this.setState({tasks: this.state.tasks.filter(item => item.id !== id)})
  //   }catch(err) {
  //     console.log(err)
  //   }
  // }

  componentDidMount() {
    this.getAllTask()
  }

  render() {
    return(
      <Fragment>
        <h1>Dashboard page</h1>
        <button onClick={this.logout}>logout</button>
        <AddForm getAll={this.getAllTask}/>
        {/* <ul>
          {this.state.tasks.map(item => 
            <li key={item.name}>
              {item.description}
              <button onClick={() => this.deleteItem(item.id)}>delete</button>
            </li>
          )}
        </ul> */}
      </Fragment>
    )
  }
}
  

export default CountryDetail;
