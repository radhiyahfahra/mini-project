import React, { Fragment, useState } from 'react';
import "../../styles/DashPage.css";
import { Link } from "react-router-dom";
import axios from "axios";
import { Form, FormGroup, CardTitle, Button, Input} from 'reactstrap'
import Swal from 'sweetalert2';

const baseUrl = "https://miniproject-team-a.herokuapp.com/api/v1/"



class DashboardPage extends React.Component {
    state = {
        name: "",
        description: "",
        deadline: "01/01/2021 10:30",
        isLoading: false,
        data: []
    }

logout = e => {
    localStorage.removeItem("token");
    this.props.history.push("/signin")
  }


  componentDidMount() {
    let token = localStorage.getItem("token")
    if(!token) {
        this.props.history.replace("/signin")
    }
  }

change = e => {
    this.setState({
      [e.target.name] : e.target.value
    })
  } 

  addTask = async(e) => {
    this.setState({ isLoading: true })
    let token = localStorage.getItem("token")
    e.preventDefault()
    // create data 
    const newTodo = {
      name: this.state.name,
      description: this.state.description,
      deadline: this.state.deadline
    }
    let updateTodo = [...this.state.data, newTodo]

    try {
      const res = await axios.post(`${baseUrl}/tasks`, newTodo, {
        headers: {
            Authorization : token
        }
      })
 //     this.props.getAll()
      this.setState({ data: updateTodo, name: "", description: "", isLoading: false })
    }
    catch(error){
      console.log(error)
    }
  }

// addTask = (e) => {
//     e.preventDefault()
//     let token = localStorage.getItem("token")
//     console.log('loading..')

//     this.setState({ loading: true})
//     axios({
//         method: "POST",
//         url : "https://miniproject-team-a.herokuapp.com/api/v1/tasks",
//         headers: {
//             Authorization: token
//           },
//           data: {
//             name: this.state.name,
//             description: this.state.description,
//             deadline: this.state.deadline
//           }
//     })
//     .then(res => {
//         this.setState({ loading: false})
//         if(res.status === 200) {
//             console.log(res.data)
//             // Swal.fire({
//             //     icon: 'success',
//             //     text: 'task berhasil ditambahkan'
//             //})
//         }
//     })
//     .catch (err => {
//         console.log(err)

//         // this.setState({ loading: false})

//         // Swal.fire({
//         //     icon: 'error',
//         //     text: err.response.message
//         // })
//     })
// }




render() {
    const {name, description, deadline, change, submit} = this.props
    return (
        <Fragment>
        <div className="dash-todos1">
            
            <div className="todos-dash">
                <ul>
                    <h4> Todos</h4>           
                    <Link to="/signin">
                        <button onCLick ={this.logout} className="signout-bot">SIGN OUT</button>
                    </Link>
                </ul>
            </div>
   
            <div className="dash-list">
                <ul>
                    <li to='/' className="myday"> My Day</li>
                    <li to='/' className="important"> Important</li>
                    <li to='/' className="completed"> Completed</li>
                </ul>

                <Form>
            <FormGroup> 
                <Input className="todo-task" name="name"
                type="text"
                placeholder = "Name" 
                value= {this.state.name} 
                onChange={this.change}/>
                <Input className="todo-desc"  name="description"
                type="text"
                placeholder = "Description" 
                value= {this.state.description} 
                onChange={this.change}/>
                {/* <Input className="todo-date" 
                type="text" name="deadline"
                placeholder = "Deadline" 
                value= {this.state.deadline} 
                onChange={this.change}/> */}
                <Button color="primary" className="add-task-bot" onClick={this.addTask}>+ Add</Button>
            </FormGroup>    
        </Form>

            <div className ="layout-list">
                LIST
                {this.state.data.map(item =>
                    <ul>
                        <li>
                            <input type="checkbox"></input>
                            <div>{item.name}</div>
                            <div>{item.description}</div>
                        </li>
                    </ul> )}
                </div>
           </div>
        </div>  
        </Fragment>
    )
}
}

export default DashboardPage;