import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import SignIn from './modules/Login/SignIn';
import SignUp from './modules/Login/SignUp';
import Routes from './routes/Routes'

import PrivateRoute from './routes/PrivateRoute'
import DashboardPage from './modules/Login/DashboardPage';


function App() {
  return (
    <React.Fragment>
      <Router>
        <Switch>
          <Routes/>
          <PrivateRoute path="/" component={SignUp} exact/>
          <PrivateRoute path="/dashboard" component={DashboardPage} exact />
          <Route path="/signin" component={SignIn} exact/>
          <Route component={() => "404 Not Found"} />
        </Switch>
      </Router>
    </React.Fragment>
  );
}

export default App;