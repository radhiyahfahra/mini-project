import React, {Fragment} from "react";
import { Route } from "react-router-dom";
import  SignUp from "../modules/Login/SignUp";
import SignIn from "../modules/Login/SignIn";
import DashboardPage from "../modules/Login/DashboardPage";
import CountryDetail from "../modules/Login/CountryDetail";
import AddTodo from "../ref/AddTodo";
import TaskTodo from "../ref/TaskTodo";

const Routes = () => {
    return (
        <div>
            <Fragment>
                <Route path="/" exact component={SignUp}/>
                <Route path="/signup" exact component={SignUp}/>
                <Route path="/signin" exact component={SignIn}/>
                <Route path="/dashboard" exact component={DashboardPage}/>
                <Route path="/test" exact component={CountryDetail}/>
                <Route path="/ref" exact component={TaskTodo}/>
             
                </Fragment>
        </div>
    )
}

export default Routes;