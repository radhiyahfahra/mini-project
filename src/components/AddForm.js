import React, {Fragment} from "react";
import axios from 'axios'
const baseUrl = "https://miniproject-team-a.herokuapp.com/api/v1"

class AddForm extends React.Component{
 state = {
    name: "",
    description: "",
//    deadline: "",
    isLoading: false
  }

  change = e => {
    this.setState({
      [e.target.name] : e.target.value
    })
  } 

  submit = async(e) => {
    this.setState({ isLoading: true })
    let token = localStorage.getItem("token")
    e.preventDefault()
    // create data 
    const data = {
      name: this.state.name,
      description: this.state.description,
//      deadline: this.state.deadline
    }

    try {
      const res = await axios.post(`${baseUrl}/tasks`, data, {
        headers: {
          auth: token
        }
      })
      this.props.getAll()
      this.setState({ name: "", description: "", isLoading: false })
    }catch(error){
      console.log(error)
    }
  }

  render() {
    return(
      <Fragment>
        <h1>add new todo</h1>
        <form onSubmit={this.submit}>
          <input type="text" name="name" value={this.state.name} placeholder="name"  onChange={this.change}/>
          <input type="text" name="description" value={this.state.description} placeholder="description" onChange={this.change}/>
          
          <button  type='submit'>{this.state.isLoading ? "loading..." : "add"}</button>
        </form>
      </Fragment>
    )
  }  
}

export default AddForm;
